
 Esta prueba fue desarrollada usando el template de vite de typescript el cual tiene como objetivo mosterar los siguientes elementos:

 - Primer Nav:  El componente que lo reenderiza es MainNav
 - Segundo Nav con Menu: El componente que lo reenderiza es MenuNav, este componente se encarga de obtener los datos con axios y reenderizarlos, se decidio usar useState puesto que solo se necesita que los datos obtenidos se muestren en este componente, pues si se hace desde algún padre se reenderizará innecesariamente otros componentes como el footer o el contenido.

 - Un contenedor con un background: El componente que lo reenderiza es ContentContainer.
 - Un footer con marcas y copyright: El componente que lo reenderiza es Footer.


 Funciones dinamicas:
 
 - Construir el menú dinámicamente por medio de un servicio:
 
 Como habiamos mostrado anteriormente se usa axios para obtener los datos desde el segundo navbar una vez este se haya reenderizado
 
 - Este menú debe contener un primer nivel y “n” subniveles: Para solucionar este se hace uso de la recursividad en el componente RecursiveItemMenu para para los items que no pertenecen al navbar y si al dropdown, para los items del navbar se hace uso del componente HoverItem y menuNav (padre), el primero se encarga de mostrar las animaciones y mostrar el dropdown inicial para posteriormente llamar el componente RecursiveItemMenu


- El texto de los ítems del menú deben renderizarse con base al key de cada uno de los objetos: Se hace uso de un diccionario  en el archivo utils/translate.ts para obtnener las respectivas traducciones de las keys, se usa frecuentemente Object.entries para obtener las keys y asi traducirlas, en caso de que la key no exista se obtendra el texto "No encontrado"




### Cómo correr el proyecto?
Una vez instalado node puedes correr el proyecto siguiendo los comandos presentados a continuación


```console
yarn or npm i
```

#### dev mode

```console
yarn dev or npm dev
```

#### build and preivew

```console
yarn build or npm build
```

```console
yarn preview or npm preview
```

Una vez ejecutado estos comandos deberias ser capaz de ver
http://127.0.0.1:5173/ si estas en dev mode o http://127.0.0.1:4173/ si estan en el entorno de producción


###  preview

Puedes ver el proyecto desplegado en https://dye6co9doqvq9.cloudfront.net


