const defaultText = "No encontrado";

const translator: Record<string, string> = {
  card_list: "Lista de tarjetas",
  pays_transfer: "Transferencias y pagos",
  between_cards: "Entre tarjetas",
  banks: "Cuenta bancaria",
  credit_card: "Tarjeta de credito",
  visa: "visa",
  master_card: "master card",
  support: "Atención al cliente",
  lock: "Bloqueos",
  change_pass: "Cambio de clave",
  enterprises: "Empresas",
};

export const getTranslate = (val: string) => {
  const translated = translator[val] || defaultText;
  return translated;
};

export default translator;
