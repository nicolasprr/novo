import { createGlobalStyle } from "styled-components";

const baseSpacing = 8;

export const theme = {
  colors: {
    primary: "#1804a4",
    secondary: "#a79fce",
    gray: {
      primary: "#e1e1e1",
      secondary: "#82777b",
    },
  },
  sizes: {
    nav: {
      main: {
        height: "35px",
      },
    },
  },
  spacing: {
    base: (...args: number[]) =>
      args.map((space) => `${space * baseSpacing}px`).join(" "),
  },
};

export type ThemeType = typeof theme;

export const GlobalStyle = createGlobalStyle`
    body {
      margin: 0px;
      min-height: 100vh;
      width: 100%;
    }
    #root {
      height: 100vh;
      display: flex;
      flex-direction: column;
    }
`;
