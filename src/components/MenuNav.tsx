import { useEffect, useState } from "react";
import styled from "styled-components";
import axios, { AxiosResponse } from "axios";

import { getTranslate } from "../utils/translate";
import HoverItem from "./HoverItem";

const NavContainer = styled.nav`
  height: fit-content;
  background-color: white;
  padding: ${({ theme }) => theme.spacing.base(0, 4)};
  display: flex;
  border-bottom: ${({ theme }) => `1px solid ${theme.colors.gray.primary}`};
  color: white;
  align-items: center;
`;

const ListContainer = styled.ul`
  display: flex;
  width: 100%;
  margin: 0;
  gap: ${({ theme }) => theme.spacing.base(1.5)};
  color: ${({ theme }) => theme.colors.gray.secondary};
  font-weight: 600;
  list-style-type: none;
`;

interface IItem {
  [k: string]: [IItem] | [];
}

const MenuNav = () => {
  const [lists, setList] = useState<IItem>({});

  useEffect(() => {
    const getMenuAPI = async () => {
      const response = (await axios.get(
        "https://services-ten.vercel.app/menu"
      )) as AxiosResponse<{ data: { list: [IItem] } }>;
      const data = response.data?.data?.list?.[0] || {};
      setList(data);
    };
    getMenuAPI();
  }, []);

  return (
    <NavContainer>
      <ListContainer>
        {Object.entries(lists).map(([key, val]) => (
          <HoverItem key={key} itemsMenu={val?.[0] || {}}>
            {getTranslate(key)}
          </HoverItem>
        ))}
      </ListContainer>
    </NavContainer>
  );
};
export default MenuNav;
