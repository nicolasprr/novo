import styled from "styled-components";

import Prutec from "../assets/Prutec.svg";
import Arrow from "../assets/arrow.svg";

const NavContainer = styled.div`
  height: fit-content;
  background-color: ${({ theme }) => theme.colors.primary};
  padding: ${({ theme }) => theme.spacing.base(1, 3.5)};
  display: flex;
  color: white;
  align-items: center;
`;

const UserContainer = styled.div`
  margin-left: auto;
  display: flex;
  gap: ${({ theme }) => theme.spacing.base(1.5)};
`;

const MainNav = () => {
  return (
    <NavContainer>
      <img src={Prutec} />
      <UserContainer>
        <p>USERNAME</p>
        <img src={Arrow} />
      </UserContainer>
    </NavContainer>
  );
};
export default MainNav;
