import { motion } from "framer-motion";
import styled from "styled-components";

interface Props {
  direction: "bottom" | "right";
}

const DropdownMenu = styled(motion.div)<Props>`
  border-radius: 4px;
  width: 100%;
  height: fit-content;
  border: 1px solid black;
  position: absolute;
  background-color: white;
  top: ${({ direction }) => (direction === "bottom" ? "42px" : "0")};
  left: ${({ direction }) => (direction === "right" ? "100%" : "none")};
  margin-left: ${({ direction }) => (direction === "right" ? "2px" : "none")};
  border-bottom-width: 0;
`;

export default DropdownMenu;
