import styled from "styled-components";

import Isotopo from "../assets/Isotipo.svg";

const FooterContainer = styled.footer`
  height: 25%;
  width: 100%;
  display: flex;
  justify-content: space-around;
  align-items: center;
  color: white;
  background-color: ${({ theme }) => theme.colors.primary};
  border-top: ${({ theme }) => `3px solid ${theme.colors.secondary}`};
  margin-top: auto;
`;

const Footer = () => {
  return (
    <FooterContainer>
      <img src={Isotopo} />
      <p> 2020 Novopayment Inc. All rights reserved</p>
      <img src={Isotopo} />
    </FooterContainer>
  );
};

export default Footer;
