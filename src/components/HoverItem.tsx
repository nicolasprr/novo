import { ReactNode, useEffect, useState } from "react";

import styled, { useTheme } from "styled-components";
import { motion, useAnimationControls } from "framer-motion";
import { IItem } from "../types";
import MainDropDownMenu from "./MainDropdownMenu";

const MenuItem = styled(motion.li)`
  font-size: 1.2rem;
  display: flex;
  position: relative;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  padding: ${({ theme }) => theme.spacing.base(1, 1, 0, 1)};
  gap: ${({ theme }) => theme.spacing.base(1)};
`;

const BottomBorder = styled(motion.div)`
  width: 0px;
  height: 4px;
`;

interface Props {
  children: ReactNode;
  itemsMenu: IItem;
}

const HoverItem = ({ children, itemsMenu }: Props) => {
  const [hover, setHover] = useState(false);
  const controls = useAnimationControls();

  const { colors } = useTheme();

  useEffect(() => {
    controls.start({
      width: hover ? "100%" : 0,
      backgroundColor: colors.secondary,
      transition: { duration: 0.3, ease: "circOut" },
    });
  }, [hover]);

  const items = itemsMenu;

  return (
    <MenuItem
      onHoverStart={() => setHover(true)}
      onHoverEnd={() => setHover(false)}
    >
      <span>{children}</span>
      <BottomBorder animate={controls} />
      <MainDropDownMenu isParentHover={hover} items={items} />
    </MenuItem>
  );
};

export default HoverItem;
