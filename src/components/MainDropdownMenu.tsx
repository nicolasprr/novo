import { AnimatePresence } from "framer-motion";
import { IItem } from "../types";
import { getTranslate } from "../utils/translate";
import DropdownMenu from "./Dropdown";
import RecursiveItemMenu from "./RecursiveItemMenu";

interface Props {
  isParentHover: boolean;
  items: IItem;
  direction?: "right" | "bottom";
}

const MainDropDownMenu = ({ isParentHover, items }: Props) => {
  const entryItems = Object.entries(items);
  return (
    <AnimatePresence>
      {isParentHover && entryItems.length > 0 && (
        <DropdownMenu
          direction="bottom"
          initial={{
            opacity: 0,
          }}
          animate={{
            opacity: 1,
          }}
          exit={{
            opacity: 0,
          }}
        >
          {entryItems.map(([key, val]) => (
            <RecursiveItemMenu
              key={key}
              name={getTranslate(key)}
              items={val?.[0] || {}}
            />
          ))}
        </DropdownMenu>
      )}
    </AnimatePresence>
  );
};

export default MainDropDownMenu;
