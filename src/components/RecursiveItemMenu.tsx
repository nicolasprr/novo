import { AnimatePresence, motion } from "framer-motion";
import { useState } from "react";
import styled from "styled-components";
import { IItem } from "../types";
import { getTranslate } from "../utils/translate";
import DropdownMenu from "./Dropdown";

const ItemMenu = styled(motion.div)`
  padding: ${({ theme }) => theme.spacing.base(1.5, 2)};
  border-bottom: 1px solid black;
  position: relative;
`;

interface Props {
  name: string;
  items: IItem;
}
const RecursiveItemMenu = ({ items, name }: Props) => {
  const [hover, setHover] = useState(false);

  const entryItems = Object.entries(items);

  return (
    <ItemMenu
      onHoverStart={() => setHover(true)}
      onHoverEnd={() => setHover(false)}
    >
      {name}
      <AnimatePresence>
        {hover && entryItems.length > 0 && (
          <DropdownMenu
            direction="right"
            initial={{
              opacity: 0,
            }}
            animate={{
              opacity: 1,
            }}
            exit={{
              opacity: 0,
            }}
          >
            {entryItems.map(([key, val]) => (
              <RecursiveItemMenu
                key={key}
                name={getTranslate(key)}
                items={val?.[0] || {}}
              />
            ))}
          </DropdownMenu>
        )}
      </AnimatePresence>
    </ItemMenu>
  );
};

export default RecursiveItemMenu;
