import styled from "styled-components";
import bg from "../assets/bg-content.jpg";

const ContentContainer = styled.div`
  width: 100%;
  height: 100%;
  background-image: url(${bg});
  background-repeat: repeat-x;
`;

export default ContentContainer;
