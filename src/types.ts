export interface IItem {
  [k: string]: [IItem] | [];
}
