import { ThemeProvider } from "styled-components";
import ContentCotnainer from "./components/ContentContainer";
import Footer from "./components/Footer";
import MainNav from "./components/MainNav";
import MenuNav from "./components/MenuNav";
import { GlobalStyle, theme } from "./theme";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle/>
      <MainNav />
      <MenuNav />
      <ContentCotnainer />
      <Footer />
    </ThemeProvider>
  );
}

export default App;
